<?php 
/**
 * @file
 * Admin functions module
 *
 * This module creates entities, controllers and permissions
 */

module_load_include('inc', 'cloudsms_admin', 'inc/controllers');

module_load_include('inc', 'cloudsms_admin', 'inc/entities');

module_load_include('inc', 'cloudsms_admin', 'inc/permissions');

module_load_include('inc', 'cloudsms_admin', 'inc/helpers/functions');

/**
 * Implements hook_menu().
 */
function cloudsms_admin_menu() {

  $items = array();

  $items['admin/cloudsms'] = array(
    'title' => 'CloudSMS',
    'description' => 'Administer CloudSMS',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer cloudsms'),
    'file path' => drupal_get_path('module', 'system'),
    'file' => 'system.admin.inc',
    'weight' => 4,
  );

  $items['admin/cloudsms/settings'] = array(
    'title' => 'CloudSMS Settings',
    'description' => 'Enter CloudSMS Settings for the ClockworkSMS API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cloudsms_admin_settings'),
    'file' => 'inc/forms/settings/form.inc',
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('administer cloudsms'),
  );

  $items['admin/cloudsms/list/%cloudsms_admin_list'] = array(
    'title' => 'List',
    'page callback' => 'cloudsms_admin_list_view_entity',
    'page arguments' => array(3),
    'access arguments' => array('administer cloudsms'),
  );

  $items['admin/cloudsms/message/%cloudsms_admin_message'] = array(
    'title' => 'Message',
    'page callback' => 'cloudsms_admin_message_view_entity',
    'page arguments' => array(3),
    'access arguments' => array('administer cloudsms'),
  );

  $items['admin/cloudsms/subscriber/%cloudsms_admin_subscriber'] = array(
    'title' => 'Subscriber',
    'page callback' => 'cloudsms_admin_subscriber_view_entity',
    'page arguments' => array(3),
    'access arguments' => array('administer cloudsms'),
  );

  $items['admin/cloudsms/subscribers/import'] = array(
    'title' => 'Import Subscribers',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cloudsms_admin_subscriber_import_form'),
    'access arguments' => array('administer cloudsms'),
    'file' => 'inc/forms/subscriber/import.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_load().
 */
function cloudsms_admin_list_load($id) {
  $list = entity_load('cloudsms_list', array($id));
  return array_pop($list);
}

/**
 * Implements hook_view_entity().
 */
function cloudsms_admin_list_view_entity($list) {
  $entity = entity_view('cloudsms_list', array($list->lid => $list));
  return $entity;
}

/**
 * Implements hook_load().
 */
function cloudsms_admin_message_load($id) {
  $list = entity_load('cloudsms_message', array($id));
  return array_pop($list);
}

/**
 * Implements hook_view_entity().
 */
function cloudsms_admin_message_view_entity($message) {
  $entity = entity_view('cloudsms_message', array($message->mid => $message));
  return $entity;
}

/**
 * Implements hook_load().
 */
function cloudsms_admin_subscriber_load($id) {
  $list = entity_load('cloudsms_subscriber', array($id));
  return array_pop($list);
}

/**
 * Implements hook_view_entity().
 */
function cloudsms_admin_subscriber_view_entity($subscriber) {
  $entity = entity_view('cloudsms_subscriber', array($subscriber->sid => $subscriber));
  return $entity;
}

/**
 * Implements hook_entity_info().
 */
function cloudsms_admin_entity_info() {

  $info = array();

  $info['cloudsms_list'] = array(
    'label' => t('List'),
    'base table' => 'cloudsms_lists',
    'entity keys' => array(
      'id' => 'lid',
      'label' => 'name',
    ),
    'uri callback' => 'entity_class_uri',
    'entity class' => 'CloudSMSAdminList',
    'controller class' => 'CloudSMSAdminListController',
    'admin ui' => array(
      'path' => 'admin/cloudsms/lists',
      'controller class' => 'CloudSMSAdminListUIController',
      'menu wildcard' => '%cloudsms_admin_list',
      'file' => 'inc/forms/list/form.inc',
    ),
    'access callback' => 'cloudsms_admin_access_callback',
    'module' => 'cloudsms_admin',
    'views controller class' => 'EntityDefaultViewsController',
  );

  $info['cloudsms_message'] = array(
    'label' => t('Message'),
    'base table' => 'cloudsms_messages',
    'entity keys' => array(
      'id' => 'mid',
      'label' => 'message',
    ),
    'uri callback' => 'entity_class_uri',
    'entity class' => 'CloudSMSAdminMessage',
    'controller class' => 'CloudSMSAdminMessageController',
    'admin ui' => array(
      'path' => 'admin/cloudsms/messages',
      'controller class' => 'CloudSMSAdminMessageUIController',
      'menu wildcard' => '%cloudsms_admin_message',
      'file' => 'inc/forms/message/form.inc',
    ),
    'access callback' => 'cloudsms_admin_access_callback',
    'module' => 'cloudsms_admin',
    'views controller class' => 'EntityDefaultViewsController',
  );

  $info['cloudsms_subscriber'] = array(
    'label' => t('Subscriber'),
    'base table' => 'cloudsms_subscribers',
    'entity keys' => array(
      'id' => 'sid',
      'label' => 'number',
    ),
    'uri callback' => 'entity_class_uri',
    'entity class' => 'CloudSMSAdminSubscriber',
    'controller class' => 'CloudSMSAdminSubscriberController',
    'admin ui' => array(
      'path' => 'admin/cloudsms/subscribers',
      'controller class' => 'CloudSMSAdminSubscriberUIController',
      'menu wildcard' => '%cloudsms_admin_subscriber',
      'file' => 'inc/forms/subscriber/form.inc',
    ),
    'access callback' => 'cloudsms_admin_access_callback',
    'module' => 'cloudsms_admin',
    'views controller class' => 'EntityDefaultViewsController',
  );

  return $info;
}

/**
 * Implements hook_cron().
 */
function cloudsms_admin_cron() {

  $queue = DrupalQueue::get('send_sms_task');

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'cloudsms_message')
    ->propertyCondition('send', time(), '<=');

  $results = $query->execute();

  if (isset($results['cloudsms_message'])) {
    $nids = array_keys($results['cloudsms_message']);
    $messages = entity_load('cloudsms_message', $nids);
    foreach ($messages as $message) {
      if ($message->sent == 0) {
        $queue->createItem($message);
      }
    }
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function cloudsms_admin_cron_queue_info() {
  $queues = array();
  $queues['send_sms_task'] = array(
    'worker callback' => 'cloudsms_admin_sms_cron_task',
    'time' => 120,
  );
  return $queues;
}

/**
 * Task to send sms called by cron.
 *
 * @param object $message
 *   The message to send
 */
function cloudsms_admin_sms_cron_task($message) {

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'cloudsms_subscriber')
    ->propertyCondition('lid', $message->lid);

  $results = $query->execute();

  if (isset($results['cloudsms_subscriber'])) {

    $nids = array_keys($results['cloudsms_subscriber']);

    $subscribers = entity_load('cloudsms_subscriber', $nids);

    $messages = array();

    foreach ($subscribers as $subscriber) {

      $messages[] = array(
        'to' => $subscriber->number,
        'message' => cloudsms_admin_replace_message_tokens($message->message, $subscriber),
      );

    }

    if (cloudsms_admin_estimate_message_cost($messages) <= cloudsms_balance_messages()) {

      if (count($subscribers) <= 50) {
        $results = cloudsms_send_sms($messages);
      }
      else {
        $results = cloudsms_send_batch_sms($messages);
      }

      if ($results['status'] == 'success') {
        $message->sent = time();
        $message->save();
        watchdog('info',
          'Message @id was sent successfully',
          array(
            '@id' => $message->mid,
          )
        );
      }
      else {
        watchdog('error',
          'Message @id was not sent successfully <pre>@errors</pre>',
          array(
            '@id' => $message->mid,
            '@errors' => print_r($results, TRUE),
          )
        );
      }

    }
    else {
      watchdog('error',
        'Message @id was not sent as insufficient credit to send',
        array(
          '@id' => $message->mid,
        )
      );
    }
  }
}

/**
 * Implements hook_views_data_alter().
 */
function cloudsms_admin_views_data_alter(&$data) {

  $data['cloudsms_lists']['user'] = array(
    'title' => t('Relationship to Users'),
    'help' => t('Add a relationship to the Users table'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Relationship between list and user'),
      'title' => t('Relationship between list and user'),
      'help' => t('Creates a relationship between the list and user by using the UID field on list'),
    ),
  );

  $data['cloudsms_messages']['list'] = array(
    'title' => t('Relationship to List'),
    'help' => t('Add a relationship to the Lists table'),
    'relationship' => array(
      'base' => 'cloudsms_lists',
      'base field' => 'lid',
      'field' => 'lid',
      'handler' => 'views_handler_relationship',
      'label' => t('Relationship between message and list'),
      'title' => t('Relationship between message and list'),
      'help' => t('Creates a relationship between a message and list by using the LID field on message'),
    ),
  );

  $data['cloudsms_subscribers']['list'] = array(
    'title' => t('Relationship to List'),
    'help' => t('Add a relationship to the Lists table'),
    'relationship' => array(
      'base' => 'cloudsms_lists',
      'base field' => 'lid',
      'field' => 'lid',
      'handler' => 'views_handler_relationship',
      'label' => t('Relationship between subscriber and list'),
      'title' => t('Relationship between subscriber and list'),
      'help' => t('Creates a relationship between a subscriber and list by using the LID field on subscriber'),
    ),
  );

}

/**
 * Implements entity_property_info().
 */
function cloudsms_admin_entity_property_info() {

  $info = array();

  $info['cloudsms_list']['properties']['lid'] = array(
    'label' => t('List ID'),
    'description' => t('The unique List ID of the list'),
    'type' => 'integer',
    'schema field' => 'lid',
  );
  $info['cloudsms_list']['properties']['uid'] = array(
    'label' => t('User ID'),
    'description' => t('The user ID owning this list'),
    'type' => 'user',
    'schema field' => 'uid',
  );
  $info['cloudsms_list']['properties']['name'] = array(
    'label' => t('Name'),
    'description' => t('The name of this list'),
    'type' => 'text',
    'schema field' => 'name',
  );
  $info['cloudsms_list']['properties']['created'] = array(
    'label' => t('Created'),
    'description' => t('The date this list was created'),
    'type' => 'date',
    'schema field' => 'created',
  );
  $info['cloudsms_list']['properties']['updated'] = array(
    'label' => t('Updated'),
    'description' => t('The date this list was updated'),
    'type' => 'date',
    'schema field' => 'updated',
  );
  $info['cloudsms_message']['properties']['mid'] = array(
    'label' => t('Message ID'),
    'description' => t('The unique Message ID of the message'),
    'type' => 'integer',
    'schema field' => 'mid',
  );
  $info['cloudsms_message']['properties']['lid'] = array(
    'label' => t('List ID'),
    'description' => t('The List ID of the message list'),
    'type' => 'integer',
    'schema field' => 'lid',
  );
  $info['cloudsms_message']['properties']['message'] = array(
    'label' => t('Message'),
    'description' => t('The message'),
    'type' => 'integer',
    'schema field' => 'message',
  );
  $info['cloudsms_message']['properties']['send'] = array(
    'label' => t('Send'),
    'description' => t('The date to send the message'),
    'type' => 'date',
    'schema field' => 'send',
  );
  $info['cloudsms_message']['properties']['created'] = array(
    'label' => t('Created'),
    'description' => t('The date this message was created'),
    'type' => 'date',
    'schema field' => 'created',
  );
  $info['cloudsms_message']['properties']['updated'] = array(
    'label' => t('Updated'),
    'description' => t('The date this message was updated'),
    'type' => 'date',
    'schema field' => 'updated',
  );
  $info['cloudsms_message']['properties']['sent'] = array(
    'label' => t('Sent'),
    'description' => t('The date this message was updated'),
    'type' => 'date',
    'schema field' => 'sent',
  );
  $info['cloudsms_subscriber']['properties']['sid'] = array(
    'label' => t('Subscriber ID'),
    'description' => t('The Subscriber ID of the message'),
    'type' => 'integer',
    'schema field' => 'sid',
  );
  $info['cloudsms_subscriber']['properties']['lid'] = array(
    'label' => t('List ID'),
    'description' => t('The List ID of the subscriber'),
    'type' => 'integer',
    'schema field' => 'lid',
  );
  $info['cloudsms_subscriber']['properties']['number'] = array(
    'label' => t('Mobile Number'),
    'description' => t('The mobile number of the subscriber'),
    'type' => 'text',
    'schema field' => 'number',
  );
  $info['cloudsms_subscriber']['properties']['title'] = array(
    'label' => t('Title'),
    'description' => t('The title prefix for name'),
    'type' => 'text',
    'schema field' => 'title',
  );
  $info['cloudsms_subscriber']['properties']['forename'] = array(
    'label' => t('Forename'),
    'description' => t('The forename for the subscriber'),
    'type' => 'text',
    'schema field' => 'forename',
  );
  $info['cloudsms_subscriber']['properties']['surname'] = array(
    'label' => t('Surname'),
    'description' => t('The surname for the subscriber'),
    'type' => 'text',
    'schema field' => 'surname',
  );
  $info['cloudsms_subscriber']['properties']['field1'] = array(
    'label' => t('Field1'),
    'description' => t('The first optional field'),
    'type' => 'text',
    'schema field' => 'field1',
  );
  $info['cloudsms_subscriber']['properties']['field2'] = array(
    'label' => t('Field2'),
    'description' => t('The second optional field'),
    'type' => 'text',
    'schema field' => 'field2',
  );
  $info['cloudsms_subscriber']['properties']['field3'] = array(
    'label' => t('Field3'),
    'description' => t('The third optional field'),
    'type' => 'text',
    'schema field' => 'field3',
  );
  $info['cloudsms_subscriber']['properties']['field4'] = array(
    'label' => t('Field4'),
    'description' => t('The fourth optional field'),
    'type' => 'text',
    'schema field' => 'field4',
  );
  $info['cloudsms_subscriber']['properties']['field5'] = array(
    'label' => t('Field5'),
    'description' => t('The fifth optional field'),
    'type' => 'text',
    'schema field' => 'field5',
  );
  $info['cloudsms_subscriber']['properties']['created'] = array(
    'label' => t('Created'),
    'description' => t('The date this subscriber was created'),
    'type' => 'date',
    'schema field' => 'created',
  );
  $info['cloudsms_subscriber']['properties']['updated'] = array(
    'label' => t('Updated'),
    'description' => t('The date this subscriber was updated'),
    'type' => 'date',
    'schema field' => 'updated',
  );
  return $info;
}
