<?php
/**
 * @file
 * Permission and permission callback
 *
 * This file creates the permission for administering and provides callback
 */

/**
 * Implements hook_permission().
 */
function cloudsms_admin_permission() {
  return array(
    'administer cloudsms' => array(
      'title' => t('Administer CloudSMS'),
    ),
  );
}

/**
 * Callback function to check user has permission.
 *
 * @param string $op
 *   The operation
 * @param object $entity
 *   The entity you are checking
 * @param object $account
 *   The user account to check against
 *
 * @return bool
 *   User does or doesn't have permission
 */
function cloudsms_admin_access_callback($op, $entity = NULL, $account = NULL) {
  if (user_access('administer cloudsms', $account)) {
    return TRUE;
  }
  return FALSE;
}
