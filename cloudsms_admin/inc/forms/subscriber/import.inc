<?php
/**
 * @file
 * Form and submit callback for importing subscribers form
 *
 * This module creates the form for importing subscribers via CSV
 */

module_load_include('inc', 'cloudsms_admin', 'inc/forms/helpers/options');

/**
 * Implements hook_form().
 */
function cloudsms_admin_subscriber_import_form($form, &$form_state) {

  $form = array();

  $form['intro'] = array(
    '#type' => 'markup',
    '#markup' => t('Select a CSV file to upload.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['require1'] = array(
    '#type' => 'markup',
    '#markup' => t('This file should have column names in the first row and be in the format as below.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['require2'] = array(
    '#type' => 'markup',
    '#markup' => t('All column names are required in the first row, however only the fields lid, number, title, forename & surname are required to contain a value for the rows. The list id value (lid) can be found on the <a href="/admin/cloudsms/lists">lists page</a>.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['require3'] = array(
    '#type' => 'markup',
    '#markup' => t('lid,number,title,forename,surname,field1,field2,field3,field4,field5'),
    '#prefix' => '<p><strong>',
    '#suffix' => '</strong></p>',
  );
  $form['lid'] = array(
    '#title' => t('List'),
    '#type' => 'select',
    '#options' => cloudsms_admin_list_lists(),
    '#description' => t('Select the list these subscribers should be subscribed to'),
    '#required' => TRUE,
  );
  $form['file'] = array(
    '#title' => t('CSV File'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded csv will be imported to the list of subscribers chosen'),
    '#upload_location' => 'public://subscribers/',
    '#required' => TRUE,
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
    ),
  );
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Import Subscribers'),
    ),
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function cloudsms_admin_subscriber_import_form_validate($form, &$form_state) {

  $file = file_load($form_state['values']['file']);

  if (!cloudsms_admin_csv_validate($file)) {
    form_set_error('file', t('Invalid format or missing fields for CSV file, failed import validation'));
  }

}

/**
 * Implements hook_form_submit().
 */
function cloudsms_admin_subscriber_import_form_submit($form, &$form_state) {

  $file = file_load($form_state['values']['file']);

  $items = cloudsms_admin_csv_associative($file);

  $results = array(
    'added' => 0,
    'skipped' => 0,
  );

  foreach ($items as $item) {
    if (!cloudsms_admin_number_exists($item)) {
      $entity = entity_create('cloudsms_subscriber', $item);
      if ($entity && $entity->save()) {
        $results['added'] += 1;
      }
    }
    else {
      $results['skipped'] += 1;
    }
  }

  drupal_set_message(t('@added subscribers were added, @skipped were skipped',
    array(
      '@added' => $results['added'],
      '@skipped' => $results['skipped'],
    ))
  );

  $form_state['redirect'] = 'admin/cloudsms/subscribers';

}
