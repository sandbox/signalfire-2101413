<?php
/**
 * @file
 * Form and submit callback for subscriber form
 *
 * This module creates the form for creating and editing subscribers
 */

module_load_include('inc', 'cloudsms_admin', 'inc/forms/helpers/options');

/**
 * Implements hook_form().
 */
function cloudsms_subscriber_form($form, &$form_state, $subscriber) {

  $form = array();

  $form['subscriber_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscriber Details'),
    '#weight' => -5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['subscriber_details']['lid'] = array(
    '#title' => t('List'),
    '#type' => 'select',
    '#options' => cloudsms_admin_list_lists(),
    '#default_value' => isset($subscriber->lid) ? $subscriber->lid : '',
    '#description' => t('Select the list this subscriber belongs to'),
    '#required' => TRUE,
  );
  $form['subscriber_details']['number'] = array(
    '#title' => t('Number'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->number) ? $subscriber->number : '',
    '#description' => t('Enter the mobile phone number in international number format without a leading ‘+’ or international dialling prefix such as ’00′, e.g. 441234567890'),
    '#required' => TRUE,
  );
  $form['subscriber_details']['title'] = array(
    '#title' => t('Title'),
    '#type' => 'select',
    '#options' => cloudsms_admin_list_titles(),
    '#default_value' => isset($subscriber->title) ? $subscriber->title : '',
    '#description' => t('Select the subscriber title'),
    '#required' => TRUE,
  );
  $form['subscriber_details']['forename'] = array(
    '#title' => t('Forename'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->forename) ? $subscriber->forename : '',
    '#description' => t('Enter the subscriber forename'),
    '#required' => TRUE,
  );
  $form['subscriber_details']['surname'] = array(
    '#title' => t('Surname'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->surname) ? $subscriber->surname : '',
    '#description' => t('Enter the subscriber surname'),
    '#required' => TRUE,
  );
  $form['optional_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Optional Fields'),
    '#weight' => -4,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['optional_fields']['field1'] = array(
    '#title' => t('Field 1'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->field1) ? $subscriber->field1 : '',
    '#description' => t('Enter field 1 value'),
    '#required' => FALSE,
  );
  $form['optional_fields']['field2'] = array(
    '#title' => t('Field 2'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->field2) ? $subscriber->field2 : '',
    '#description' => t('Enter field 2 value'),
    '#required' => FALSE,
  );
  $form['optional_fields']['field3'] = array(
    '#title' => t('Field 3'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->field3) ? $subscriber->field3 : '',
    '#description' => t('Enter field 3 value'),
    '#required' => FALSE,
  );
  $form['optional_fields']['field4'] = array(
    '#title' => t('Field 4'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->field4) ? $subscriber->field4 : '',
    '#description' => t('Enter field 4 value'),
    '#required' => FALSE,
  );
  $form['optional_fields']['field5'] = array(
    '#title' => t('Field 5'),
    '#type' => 'textfield',
    '#default_value' => isset($subscriber->field5) ? $subscriber->field5 : '',
    '#description' => t('Enter field 5 value'),
    '#required' => FALSE,
  );
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($subscriber->sid) ? t('Update Subscriber') : t('Save Subscriber'),
    ),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function cloudsms_subscriber_form_validate($form, &$form_state) {

  if (!ctype_digit($form_state['values']['number'])) {
    form_set_error('number', t('The number needs to be numeric only (no + or brackets)'));
  }

}

/**
 * Implements hook_form_submit().
 */
function cloudsms_subscriber_form_submit($form, &$form_state) {
  $subscriber = entity_ui_form_submit_build_entity($form, $form_state);
  $subscriber->save();
  drupal_set_message(t('Subscriber has been created / updated'));
  $form_state['redirect'] = 'admin/cloudsms/subscribers';
}
