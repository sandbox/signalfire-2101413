<?php
/**
 * @file
 * Form and submit callback for list form
 *
 * This module creates the form for creating and editing lists
 */

module_load_include('inc', 'cloudsms_admin', 'inc/forms/helpers/options');

/**
 * Implements hook_form().
 */
function cloudsms_list_form($form, &$form_state, $list) {

  $form = array();

  $form['uid'] = array(
    '#title' => t('User ID'),
    '#type' => 'select',
    '#options' => cloudsms_admin_list_users(),
    '#default_value' => isset($list->uid) ? $list->uid : '',
    '#description' => t('User ID owning this List'),
    '#required' => TRUE,
    '#weight' => -50,
  );

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => isset($list->name) ? $list->name : '',
    '#description' => t('List name'),
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($list->lid) ? t('Update List') : t('Save List'),
    ),
  );

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function cloudsms_list_form_submit($form, &$form_state) {
  $list = entity_ui_form_submit_build_entity($form, $form_state);
  $list->save();
  drupal_set_message(t('List has been created / updated'));
  $form_state['redirect'] = 'admin/cloudsms/lists';
}
