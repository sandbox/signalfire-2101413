<?php
/**
 * @file
 * Select options for forms
 *
 * This module provides the options for select elements in forms
 */

/**
 * Returns options list for forms containing users.
 *
 * @return array
 *   Users setup in the system
 */
function cloudsms_admin_list_users() {

  $options = array();

  $users = entity_load('user');

  foreach ($users as $id => $user) {
    if ($id > 0) {
      $options[$id] = $user->name;
    }
  }

  return $options;
}

/**
 * Returns options list for forms containing lists.
 *
 * @return array
 *   Lists that have been setup
 */
function cloudsms_admin_list_lists() {

  $options = array();

  $lists = entity_load('cloudsms_list');

  foreach ($lists as $id => $list) {
    $options[$id] = $list->name;
  }

  return $options;
}

/**
 * Returns list of name prefix titles for forms.
 *
 * @return array
 *   Name prefixes
 */
function cloudsms_admin_list_titles() {
  return array(
    'Mr' => 'Mr',
    'Mrs' => 'Mrs',
    'Ms' => 'Ms',
    'Miss' => 'Miss',
  );
}
