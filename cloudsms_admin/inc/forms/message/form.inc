<?php
/**
 * @file
 * Form and submit callback for message form
 *
 * This module creates the form for creating and editing messages
 */

module_load_include('inc', 'cloudsms_admin', 'inc/forms/helpers/options');

/**
 * Implements hook_form().
 */
function cloudsms_message_form($form, &$form_state, $message) {

  $form = array();

  $form['lid'] = array(
    '#title' => t('List'),
    '#type' => 'select',
    '#options' => cloudsms_admin_list_lists(),
    '#default_value' => isset($message->lid) ? $message->lid : '',
    '#description' => t('Select the list this message should be sent to'),
    '#required' => TRUE,
  );
  $form['message'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#default_value' => isset($message->message) ? $message->message : '',
    '#description' => t('Enter message text, maximum 459 characters'),
    '#required' => TRUE,
  );
  $form['send'] = array(
    '#type' => 'date_select',
    '#title' => t('Date to Send'),
    '#description' => t('Select the date to send this message'),
    '#date_format' => 'Y-m-d H:i',
    '#default_value' => isset($message->send) ? date('Y-m-d H:i', $message->send) : date('Y-m-d H:i', time()),
  );
  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($message->mid) ? t('Update Message') : t('Save Message'),
    ),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function cloudsms_message_form_validate($form, &$form_state) {
  if (strlen($form_state['values']['message']) > 459) {
    form_set_error('message', t('The length of message is greater than 459 characters'));
  }
}

/**
 * Implements hook_form_submit().
 */
function cloudsms_message_form_submit($form, &$form_state) {
  $message = entity_ui_form_submit_build_entity($form, $form_state);
  $message->send = strtotime($form_state['values']['send']);
  $message->save();
  drupal_set_message(t('Message has been created / updated'));
  $form_state['redirect'] = 'admin/cloudsms/messages';
}
