<?php
/**
 * @file
 * Form for settings
 *
 * This module creates the form for setting variables
 */

/**
 * Used for settings form.
 *
 * @return array
 *   Settings form
 */
function cloudsms_admin_settings() {

  $form = array();

  $form['cloudsms_admin_api_key'] = array(
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cloudsms_admin_api_key', ''),
    '#description' => t('Clockwork SMS API Key'),
    '#required' => TRUE,
  );

  $form['cloudsms_admin_sms_price'] = array(
    '#title' => t('SMS Price'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cloudsms_admin_sms_price', ''),
    '#description' => t('Clockwork SMS Price (in GBP)'),
    '#required' => TRUE,
  );

  return system_settings_form($form);

}
