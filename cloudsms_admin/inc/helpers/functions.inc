<?php
/**
 * @file
 * Helper functions
 *
 * Creates functions used through other files
 */

/**
 * Replace tokens in message with subscriber data.
 *
 * @param string $message
 *   The message to be sent
 * @param object $subscriber
 *   The subscriber object
 *
 * @return string
 *   The message with tokens replaced
 */
function cloudsms_admin_replace_message_tokens($message, $subscriber) {

  $result = $message;

  $tokens = array(
    '[#number#]' => $subscriber->number,
    '[#title#]' => $subscriber->title,
    '[#forename#]' => $subscriber->forename,
    '[#surname#]' => $subscriber->surname,
    '[#field1#]' => $subscriber->field1,
    '[#field2#]' => $subscriber->field2,
    '[#field3#]' => $subscriber->field3,
    '[#field4#]' => $subscriber->field4,
    '[#field5#]' => $subscriber->field5,
  );

  foreach ($tokens as $key => $value) {
    $result = str_ireplace($key, $value, $result);
  }

  return $result;

}

/**
 * Estimates the cost of sending messages based on their length.
 *
 * @param array $messages
 *   The messages to send
 *
 * @return int
 *   The messages (number) estimated to be consumed
 */
function cloudsms_admin_estimate_message_cost($messages) {

  $total = 0;

  foreach ($messages as $message) {
    $length = strlen($message['message']);
    if ($length <= 160) {
      $total += 1;
    }
    elseif ($length > 160 && $length <= 306) {
      $total += 2;
    }
    else {
      $total += 3;
    }
  }

  return $total;

}

/**
 * Open csv file and get its data.
 *
 * @param object $file
 *   The uploaded file object
 *
 * @return array
 *   The array of data to return
 */
function cloudsms_admin_load_csv($file) {

  $handle = fopen($file->uri, "r");
  $data = array();

  while (!feof($handle)) {

    $data[] = fgetcsv($handle);

  }

  fclose($handle);

  return $data;
}

/**
 * Get the csv and create as keyed associative array.
 *
 * @param object $file
 *   The uploaded file object
 *
 * @return array
 *   The array of data to return
 */
function cloudsms_admin_csv_associative($file) {

  $rows = cloudsms_admin_load_csv($file);

  $headers = reset($rows);

  $items = array();

  foreach ($rows as $key => $value) {
    if ($key != 0) {
      $item = array();
      for ($i = 0; $i < count($headers); $i++) {
        $item[$headers[$i]] = $value[$i];
      }
      $items[] = $item;
    }
  }

  return $items;

}

/**
 * Check if a number already exists on the selected list.
 *
 * @param array $item
 *   The csv item containing list id and number
 *
 * @return bool
 *   Does the number already exist
 */
function cloudsms_admin_number_exists($item) {

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'cloudsms_subscriber')
    ->propertyCondition('lid', $item['lid'])
    ->propertyCondition('number', $item['number']);

  $results = $query->execute();

  if (isset($results['cloudsms_subscriber'])) {
    return TRUE;
  }

  return FALSE;

}

/**
 * Validate the fields of the CSV file.
 *
 * @param array $rows
 *   The rows of the CSV file
 * @param array $expected
 *   The columns and datatypes expected for columns in csv
 *
 * @return bool
 *   Does the file validate
 */
function cloudsms_admin_csv_validatefields($rows, $expected) {

  $errors = 0;

  foreach ($rows as $key => $value) {
    if ($key !== 0) {
      foreach ($value as $index => $field) {
        $type = $expected['types'][$index];
        switch ($type) {
          case 'digit':{
            if (!ctype_digit($field)) {
              $errors += 1;
            }
            break;
            }
          case 'string':{
            if (!ctype_alnum($field)) {
              $errors += 1;
            }
            break;
            }
          case 'stringornull':{
            if (!empty($field)) {
              if (!ctype_alnum($field)) {
                $errors += 1;
              }
            }
            break;
            }
        }
      }
    }
  }

  if ($errors === 0) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Initial validation and call the validate function on CSV.
 *
 * @param object $file
 *   The uploaded file
 *
 * @return bool
 *   Does the csv file pass validation
 */
function cloudsms_admin_csv_validate($file) {

  $rows = cloudsms_admin_load_csv($file);

  if ($rows) {

    $expected = array(
      'headers' => array(
        'lid',
        'number',
        'title',
        'forename',
        'surname',
        'field1',
        'field2',
        'field3',
        'field4',
        'field5',
      ),
      'types' => array(
        'digit',
        'digit',
        'string',
        'string',
        'string',
        'stringornull',
        'stringornull',
        'stringornull',
        'stringornull',
        'stringornull',
      ),
    );

    $headers = reset($rows);

    $differences = array_diff($expected['headers'], $headers);

    if (empty($differences)) {

      if (cloudsms_admin_csv_validatefields($rows, $expected)) {
        return TRUE;
      }

    }

  }

  return FALSE;
}
