<?php
/**
 * @file
 * Controller for CloudSMS Entities
 *
 * This module is the controller for the entities
 */

/**
 * List Controller implementing EntityAPIController
 */
class CloudSMSAdminListController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $variables = array();

    $variables['header'] = array(t('Field'), t('Value'));

    $variables['rows'][] = array(t('Name'), $entity->name);

    $account = user_load($entity->uid);

    $variables['rows'][] = array(t('User'), $account->name);

    $variables['rows'][] = array(
      t('Created'), date('d/m/Y H:i:s', $entity->created),
    );

    $variables['rows'][] = array(
      t('Updated'), date('d/m/Y H:i:s', $entity->updated),
    );

    $build = array(
      'intro' => array(
        '#type' => 'markup',
        '#markup' => t('The list is shown below'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ),
      'table' => array(
        '#type' => 'markup',
        '#markup' => theme('table', $variables),
      ),
      'button' => array(
        '#type' => 'button',
        '#attributes' => array('onclick' => 'history.back(-1);'),
        '#value' => t('Back'),
      ),
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    if (isset($entity->is_new)) {
      $entity->created = time();
    }

    $entity->updated = time();

    return parent::save($entity, $transaction);
  }
}

/**
 * Message Controller implementing EntityAPIController
 */
class CloudSMSAdminMessageController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $variables = array();

    $variables['header'] = array(t('Field'), t('Value'));

    $list = entity_load_single('cloudsms_list', $entity->lid);

    $variables['rows'][] = array(t('List'), $list->name);

    $variables['rows'][] = array(
      t('Message'), $entity->message,
    );

    $variables['rows'][] = array(
      t('Send'), date('d/m/Y H:i:s', $entity->send),
    );

    $variables['rows'][] = array(
      t('Created'), date('d/m/Y H:i:s', $entity->created),
    );

    $variables['rows'][] = array(
      t('Updated'), date('d/m/Y H:i:s', $entity->updated),
    );

    $variables['rows'][] = array(
      t('Sent'), date('d/m/Y H:i:s', $entity->sent),
    );

    $build = array(
      'intro' => array(
        '#type' => 'markup',
        '#markup' => t('The message is shown below'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ),
      'table' => array(
        '#type' => 'markup',
        '#markup' => theme('table', $variables),
      ),
      'button' => array(
        '#type' => 'button',
        '#attributes' => array('onclick' => 'history.back(-1);'),
        '#value' => t('Back'),
      ),
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    if (isset($entity->is_new)) {
      $entity->created = time();
    }

    $entity->updated = time();

    return parent::save($entity, $transaction);
  }
}

/**
 * Subscriber Controller implementing EntityAPIController
 */
class CloudSMSAdminSubscriberController extends EntityAPIController {

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    $build = parent::buildContent($entity, $view_mode, $langcode, $content);

    $variables = array();

    $variables['header'] = array(t('Field'), t('Value'));

    $list = entity_load_single('cloudsms_list', $entity->lid);

    $variables['rows'][] = array(
      t('List'), $list->name,
    );

    $variables['rows'][] = array(
      t('Number'), $entity->number,
    );

    $variables['rows'][] = array(
      t('Title'), $entity->title,
    );

    $variables['rows'][] = array(
      t('Forename'), $entity->forename,
    );

    $variables['rows'][] = array(
      t('Surname'), $entity->surname,
    );

    $variables['rows'][] = array(
      t('Field1'), $entity->field1,
    );

    $variables['rows'][] = array(
      t('Field2'), $entity->field2,
    );

    $variables['rows'][] = array(
      t('Field3'), $entity->field3,
    );

    $variables['rows'][] = array(
      t('Field4'), $entity->field4,
    );

    $variables['rows'][] = array(
      t('Field5'), $entity->field5,
    );

    $variables['rows'][] = array(
      t('Created'), date('d/m/Y H:i:s', $entity->created),
    );

    $variables['rows'][] = array(
      t('Updated'), date('d/m/Y H:i:s', $entity->updated),
    );

    $build = array(
      'intro' => array(
        '#type' => 'markup',
        '#markup' => t('The subscriber is shown below'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ),
      'table' => array(
        '#type' => 'markup',
        '#markup' => theme('table', $variables),
      ),
      'button' => array(
        '#type' => 'button',
        '#attributes' => array('onclick' => 'history.back(-1);'),
        '#value' => t('Back'),
      ),
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {

    if (isset($entity->is_new)) {
      $entity->created = time();
    }

    $entity->updated = time();

    return parent::save($entity, $transaction);
  }
}

/**
 * List Admin UI Controller implementing EntityDefaultUIController
 */
class CloudSMSAdminListUIController extends EntityDefaultUIController {}

/**
 * Message Admin UI Controller implementing EntityDefaultUIController
 */
class CloudSMSAdminMessageUIController extends EntityDefaultUIController {}

/**
 * Subscriber Admin UI Controller implementing EntityDefaultUIController
 */
class CloudSMSAdminSubscriberUIController extends EntityDefaultUIController {}
