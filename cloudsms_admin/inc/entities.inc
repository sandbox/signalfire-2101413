<?php
/**
 * @file
 * Entties for CloudSMS
 *
 * This module sets up the entities for the module
 */

/**
 * List Entity implmenting Entity
 */
class CloudSMSAdminList extends Entity {
  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    return array('path' => 'admin/cloudsms/list/' . $this->identifier());
  }

}

/**
 * Message Entity implmenting Entity
 */
class CloudSMSAdminMessage extends Entity {
  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    return array('path' => 'admin/cloudsms/message/' . $this->identifier());
  }

}

/**
 * Subscriber Entity implmenting Entity
 */
class CloudSMSAdminSubscriber extends Entity {
  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    return array('path' => 'admin/cloudsms/subscriber/' . $this->identifier());
  }

}
