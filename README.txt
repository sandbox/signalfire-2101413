The CloudSMS module itself is a wrapper around the Clockwork SMS API that
you can call from your own code.

The CloudSMS admin module allows saving of settings, creation of lists, 
messages and subscribers. Subscribers are per list and can be imported 
using CSV. Messages are sent using a cron task.

You will need to create an API key at the Clockwork site in order to use
the Clockwork SMS API.

The module requires you to download the Clockwork PHP SDK from GitHub and 
upload to the your libraries folder (requires the Libraries API). The 
module is not currently implementing hook_libraries_info so you will need 
to create an libraries.info file (named after the name of the folder you 
create to hold the library - e.g. clockwork-php.libraries.info) in your 
libraries folder. This minimal file works to get the library recognised:

title = Clockwork PHP Library
website = http://www.clockworksms.com
version = 1.3.1
files[php][] = class-Clockwork.php
