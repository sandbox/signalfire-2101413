<?php
/**
 * @file
 * Functions for the API.
 *
 * This file creates general functions for use by api.
 */

/**
 * Check if clockwork sms api key exists.
 *
 * @return bool
 *   Whether the api key has been created or not
 */
function cloudsms_key_exists() {

  if (variable_get('cloudsms_admin_api_key', NULL)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Format response as array for JSON.
 *
 * @param string $status
 *   The status of the response to send, either success or error
 * @param array|object|bool $data
 *   The data to include in this response array
 *
 * @return array
 *   The array of data to return
 */
function cloudsms_get_response($status, $data) {
  return array(
    'status' => $status,
    'data' => $data,
  );
}
